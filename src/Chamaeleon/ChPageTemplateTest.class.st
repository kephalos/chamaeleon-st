Class {
	#name : #ChPageTemplateTest,
	#superclass : #TestCase,
	#category : #'Chamaeleon-Tests'
}

{ #category : #running }
ChPageTemplateTest >> testRenderNoOp [
	| template result |
	template := ChPageTemplate
		fromString:
			'<html xmlns="http://www.w3.org/1999/xhtml" xmlns:tal="http://xml.zope.org/namespaces/tal">
  <body>
    <p tal:content="aVar" />
  </body>
</html>'.
	result := template
		render: (Dictionary newFromPairs: {('text' -> 'foo')}).
	result includesSubstring: '<p>foo</p>'
]
